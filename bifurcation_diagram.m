%% Bifurcation Diagram

generate_bifurcation_diagram_one_param(@logistic_map, 1, 4, 32000, 512)


%% Lyapunov Exponent

rs = linspace(0,4,500);
generate_lyapunov_diagram(@logistic_map, @logistic_map_der, rs, .5, 2000, ...
        700, -3, 1)
figure()
rs = linspace(3.545,4,500);
generate_lyapunov_diagram(@logistic_map, @logistic_map_der, rs, .5, 2000, ...
    700, -1, 1)


%% Generalised logistic map

generate_bifurcation_diagram_two_param1(@general_logistic_map, 0, 1, 1, 2000, 512)
figure()
generate_bifurcation_diagram_two_param1(@general_logistic_map, 0, 1, 2, 2000, 512)
figure()
generate_bifurcation_diagram_two_param1(@general_logistic_map, 0, 1, 3, 2000, 512)
figure()
generate_bifurcation_diagram_two_param1(@general_logistic_map, 0, 1, 10, 2000, 512)


%% Function definitions --- Bifurcation diagram
function y = logistic_map(x, r)
    % logisitic: computes the logistic map
    y = r.*x.*(1.-x);
end

function y = general_logistic_map(x, r, alpha)
    % logisitic: computes the logistic map
    y = (((1+alpha)^(1+1/alpha)/alpha-1)*r+1).*x.*(1-x.^alpha);
end

function cycle = generate_cycle_one_param(map, r, x_0, n_pre, n_cycle)
    % generate_cycle: Starting from x_0, reiterate the logistic map
    %   2^n times and create a list of all x_n during that iteration.
    %   If any x_i in the list equals x_{2^n} (the last element) up to an
    %   error of epsilon we say we found a cylce. If not, repeat the
    %   cycle search with x_{2^n} as a new starting point. In the case
    %   reduce the kill_count_down by one such that we don't loop
    %   indefinetely.
    if n_pre < 1
        error("n must be 1 or greater")
    end
    
    % pre-iterate the map, such that we converge closer to the periodic
    % orbits.
    for i=1:n_pre
        x_0=map(x_0,r);
    end
    
    cycle = zeros([n_cycle 1]);
    cycle(1) = map(x_0,r);
    for i=2:n_cycle
        cycle(i)=map(cycle(i-1),r);
    end
end

function cycle = generate_cycle_two_param(map, param1, param2, x_0, n_pre, n_cycle)
    % generate_cycle: Starting from x_0, reiterate the logistic map
    %   2^n times and create a list of all x_n during that iteration.
    %   If any x_i in the list equals x_{2^n} (the last element) up to an
    %   error of epsilon we say we found a cylce. If not, repeat the
    %   cycle search with x_{2^n} as a new starting point. In the case
    %   reduce the kill_count_down by one such that we don't loop
    %   indefinetely.
    if n_pre < 1
        error("n must be 1 or greater")
    end
    
    % pre-iterate the map, such that we converge closer to the periodic
    % orbits.
    for i=1:n_pre
        x_0=map(x_0, param1, param2);
    end
    
    cycle = zeros([n_cycle 1]);
    cycle(1) = map(x_0, param1, param2);
    for i=2:n_cycle
        cycle(i)=map(cycle(i-1), param1, param2);
    end
end

function generate_bifurcation_diagram_one_param(map, ...
    param_min, param_max, steps, max_cycle_size)
    % generate_bifurcation_diagram: from r_min to r_max generate all
    %   cycles of the logistic map. If a cycle is larger than
    %   max_cycle_size then use only the first max_cycle_size elements of
    %   the for the cycle
    if ~isscalar(param_min) || ~isscalar(param_max)
        error('r_min and r_max must be scalars')
    elseif ~isscalar(steps) && steps < 2
         error('stepsx must be scalars and larger than 1')
    elseif param_min > param_max
        temp = param_min; param_min = param_max; param_max = temp;
    end
    
    rs = linspace(param_min, param_max, steps);
    xs = zeros([steps max_cycle_size]);
    xs(1,:) = generate_cycle_one_param(map, rs(1), .5, 1000, ...
        max_cycle_size);
    
    for i=2:steps
        xs(i,:) = generate_cycle_one_param(map, rs(i), .5, 1000, ...
            max_cycle_size);
    end

    plot(rs,xs,'.k')    
    axis([param_min param_max 0 1])
end

function generate_bifurcation_diagram_two_param1(map, ...
    param1_min, param1_max, param2, steps, max_cycle_size)
    % generate_bifurcation_diagram: from r_min to r_max generate all
    %   cycles of the logistic map. If a cycle is larger than
    %   max_cycle_size then use only the first max_cycle_size elements of
    %   the for the cycle
    if ~isscalar(param1_min) || ~isscalar(param1_max)
        error('r_min and r_max must be scalars')
    elseif ~isscalar(steps) && steps < 2
         error('stepsx must be scalars and larger than 1')
    elseif param1_min > param1_max
        temp = param1_min; param1_min = param1_max; param1_max = temp;
    end
    
    params = linspace(param1_min, param1_max, steps);
    xs = zeros([steps max_cycle_size]);
    xs(1,:) = generate_cycle_two_param(map, params(1), param2, .5, ...
        1000, max_cycle_size);
    
    for i=2:steps
        xs(i,:) = generate_cycle_two_param(map, params(i), param2, .5, ...
            1000, max_cycle_size);
    end

    plot(params,xs,'.k')    
    axis([param1_min param1_max 0 1])
end


%% Function definitions --- Lyapunov exponent

function y = logistic_map_der(x, r)
    % logisitic: computes the logistic map
    y = r.*(1-2.*x);
end

function lambda = lyapunov_one_param(map, map_der, param, x_0, n_pre, n_cycle)
    % lyapunov_one_param: approximatively determine the
    %   Lyapunov exponent of a single-parameter-family map.
    lambda = sum(log(abs(map_der(...
        generate_cycle_one_param(map, param, x_0, n_pre, n_cycle), param )...
    )))/n_cycle;
end

function generate_lyapunov_diagram(map, map_der, params, x_0, n_pre, ...
        n_cycle, x_min, x_max)
    ls = zeros(size(params));
    for i = 1:max(size(params))
        ls(i) = lyapunov_one_param(map, map_der, params(i), x_0, n_pre, n_cycle);
    end
    
    plot(params, ls,[params(1), params(end)],[0,0])
    axis([params(1) params(end) x_min x_max])
end