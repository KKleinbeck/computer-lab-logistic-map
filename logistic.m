%% First

%generate_cycle(3.000001,.5,12,1e-5)
generate_bifurcation_diagram(3,3.8,500,128);

%% Function definitions
function y = logistic_map(x, r)
    % logisitic: computes the logistic map
    y = r*x*(1-x);
end

function cycle = generate_cycle(r, x_0, n, epsilon)
    % generate_cycle: Starting from x_0, reiterate the logistic map
    %   2^n times and create a list of all x_n during that iteration.
    %   If any x_i in the list equals x_{2^n} (the last element) up to an
    %   error of epsilon we say we found a cylce. If not, repeat the
    %   cycle search with x_{2^n} as a new starting point. In the case
    %   reduce the kill_count_down by one such that we don't loop
    %   indefinetely.
    if n < 1
        error("n must be 1 or greater")
    end
    
    cycle = zeros([2^n 1]);
    cycle(1) = logistic_map(x_0,r);
    for i=2:2^n
        cycle(i)=logistic_map(cycle(i-1),r);
    end
    
    diff = abs(cycle(1:end-1)-cycle(end));
    ind = find(diff < epsilon, 1, 'last');
    if isempty(ind)
        error("Could not find a cycle in the given number of recurrsions")
    else
        cycle = cycle(ind+1:end);
    end
end

function generate_bifurcation_diagram(r_min, r_max, steps, max_cycle_size)
    % generate_bifurcation_diagram: from r_min to r_max generate all
    %   cycles of the logistic map. If a cycle is larger than
    %   max_cycle_size then use only the first max_cycle_size elements of
    %   the for the cycle
    if ~isscalar(r_min) || ~isscalar(r_max)
        error('r_min and r_max must be scalars')
    elseif ~isscalar(steps) && steps < 2
         error('stepsx must be scalars and larger than 1')
    elseif r_min > r_max
        temp = r_min; r_min = r_max; r_max = temp;
    end
    
    rs = linspace(r_min, r_max, steps);
    xs = -ones([steps max_cycle_size]);
    xs(1,:) = change_to_length(generate_cycle(rs(1), .5, 14, 1e-3), ...
        max_cycle_size, -1);
    
    for i=2:steps
        xs(i,:) = change_to_length(generate_cycle(rs(i), xs(i-1,1), 14, 1e-3), ...
            max_cycle_size, -1);
    end

    plot(rs,xs,'.k')    
    axis([r_min r_max 0 1])
end

function array_out = change_to_length(array_in, len, val)
    % change_to_length: takes 'array_in' and creates from it 'array_out'
    %   with given size 'length' by either filling the missing places
    %   with 'val' or by throughing entries away
    array_out = array_in;
    array_size = size(array_out);
    if array_size(1) == 1
        if array_size(2) > len
            array_out = array_out(1:len);
        elseif array_size(2) < len
            array_out(array_size(2)+1:len) = val;
        end
    elseif array_size(2) == 1
        if array_size(1) > len
            array_out = array_out(1:len);
        elseif array_size(1) < len
            array_out(array_size(1)+1:len) = val;
        end
    else
        error('not an array but a matrix was given')
    end
end